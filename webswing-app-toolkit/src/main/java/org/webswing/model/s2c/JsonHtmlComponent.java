package org.webswing.model.s2c;

import java.awt.Insets;
import java.awt.Point;
import java.awt.Window;
import java.io.Serializable;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import org.webswing.util.CalcLocationOfComponent;
import org.webswing.util.Util;

public class JsonHtmlComponent implements Serializable {

    public String parentWindowID; // guiId
    public String id;
    public int x;
    public int y;
    public int w;
    public int h;
    public String content;
    public String typ;
    public String fontfam;
    public int fontSize;
    public int caret;
    public int selectionStart;
    public int selectionEnd;
    public String align;

    public JsonHtmlComponent() {
        id = "null";
    }

    public JsonHtmlComponent(String parentWindowID, final JTextField c, Window window) {
        this.parentWindowID = parentWindowID;
        CalcLocationOfComponent calc = new CalcLocationOfComponent();
        Point p = calc.calcLocationInWindow(c);
        Insets decoInsets = Util.getWebToolkit().getImageService().getWindowDecorationTheme().getInsets();
        x = p.x + decoInsets.left + window.getLocationOnScreen().x;
        y = p.y + decoInsets.top + window.getLocationOnScreen().y;

        int edgeHorizontal = decoInsets.left + decoInsets.right;
        if (c.getSize().width + p.x > window.getSize().width - edgeHorizontal) {
            w = window.getSize().width - edgeHorizontal - p.x;
        } else {
            w = c.getWidth();
        }

        int edgeVertical = decoInsets.top + decoInsets.bottom;
        if (c.getSize().height + p.y > window.getSize().height - edgeVertical) {
            h = window.getSize().height - edgeVertical - p.y;
        } else {
            h = c.getHeight();
        }

        id = "hash" + c.hashCode();
        fontfam = c.getFont().getFamily();

        content = c.getText();
        caret = c.getCaretPosition();
        selectionStart = c.getSelectionStart();
        selectionEnd = c.getSelectionEnd();
        align = getAligment(c);

        fontSize = c.getFont().getSize();
        if (c instanceof JPasswordField) {
            typ = "password";
            //fontSize +=  8;
        } else {
            typ = "text";
        }

    }

    public void validate(JTextField c, Window window) {
        Insets decoInsets = Util.getWebToolkit().getImageService().getWindowDecorationTheme().getInsets();
        int edge = decoInsets.left + decoInsets.right;
        CalcLocationOfComponent calc = new CalcLocationOfComponent();
        Point p = calc.calcLocationInWindow(c);

        if (c.getSize().width + p.x > window.getSize().width - edge) {
            w = window.getSize().width - edge - p.x;
        }
    }

    public boolean equals(JsonHtmlComponent h) {
        //  this.content.equals(h.content) sorgt fuer Probleme ... daher weglassen
        return  this.x == h.x
                && this.y == h.y
                && this.w == h.w
                && this.h == h.h
                && this.fontfam.equals(h.fontfam)
                && this.fontSize == h.fontSize
                && this.align.equals(h.align);
    }

    private String getAligment(JTextField c) {
        switch (c.getHorizontalAlignment()) {
            case SwingConstants.CENTER:
                return "center";
            case SwingConstants.RIGHT:
                return "right";
            default:
                return "left";
        }
    }
}
