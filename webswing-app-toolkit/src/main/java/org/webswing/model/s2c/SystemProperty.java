package org.webswing.model.s2c;

import org.webswing.model.c2s.JsonEvent;

public class SystemProperty implements JsonEvent {
    private String key;
    private String value;
    private String clientId;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
    
    
}
