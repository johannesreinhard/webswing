package org.webswing.model.s2c;
import java.io.Serializable;

public class JsonFileUploadAck implements Serializable{
    public String filename;

    public JsonFileUploadAck(String filename) {
        this.filename = filename;
    }
}
