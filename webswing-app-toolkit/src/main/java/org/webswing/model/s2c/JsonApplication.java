package org.webswing.model.s2c;


public class JsonApplication {
    public String name;
    public String base64Icon;
    public String args;
    public String redirect;
    
    @Override
    public String toString() {
        return "JsonApplication [name=" + name + "]";
    }
    
}
