package org.webswing.model.c2s;

public  class JsonEventKeyzz implements JsonEvent {
    public String clientId;
    public Keyzz k[];
    public int c; // last Position
 
    
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setK(Keyzz[] k) {
        this.k = k;
    }

    public void setC(int c) {
        this.c = c;
    }
   
    @Override
    public String toString() {
        String s =""; 
        for(Keyzz i : k){
            s += i + "; ";
        }
        return "JsonEventKeyzz{" + "clientId=" + clientId + ", k=" + s + ", c=" + c + '}';
    } 
}
