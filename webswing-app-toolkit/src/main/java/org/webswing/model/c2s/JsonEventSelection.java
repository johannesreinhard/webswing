package org.webswing.model.c2s;


public class JsonEventSelection implements JsonEvent {
    public String clientId;
    public String id;
    public int start;
    public int end;

    @Override
    public String toString() {
        return "JsonEventSelection{" + "clientId=" + clientId + ", id=" + id + ", start=" + start + ", end=" + end + '}';
    }
}
