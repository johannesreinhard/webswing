package org.webswing.model.c2s;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import org.webswing.dispatch.Keys;

public class Keyzz implements JsonEvent {

    int c;
    int d;
    String text;
    int k;
    
    public void setC(int c) {
        this.c = c;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setD(int d) {
        this.d = d;
    }

    public void setK(int k) {
        this.k = k;
    }

    public String getText() {
        return text;
    }

    public int getK() {
        return k;
    }

    public int getCaret() {
        return this.c;
    }
        
    public int updateCaret() {
        int newCaret = this.c;
        if (text != null) {
            newCaret = this.c + 1; 
        } else if(k == KeyEvent.VK_DELETE) { 
            newCaret = this.c;
        } else if(k == KeyEvent.VK_BACK_SPACE) {
            newCaret = this.c - d;
        }
        return newCaret;
    }

    public void postEvents(JTextField src) {
        if (text != null) {
            keypress(src);
        } else {
            keydown(src);
        }
    }

    public void vadlidateKey() {
        if (k == 13) {//enter keycode
            k = KeyEvent.VK_ENTER; // = 10
        } else if (k == 46) {//delete keycode  
            k = KeyEvent.VK_DELETE; // 127
        }
        //TODO enter und entf verschicken in java drei events im web sind es nur 2 
    }
    
    public void keydown(JTextField src) {
        vadlidateKey();
        char ch = (char) k;
        int number = (src.getSelectionEnd() - src.getSelectionStart() > 0 && src.getCaretPosition() != src.getSelectionStart()) ? 1 : d;
        //super 
        //==== count 1 caret 2 start/ end 2 - start 0
        // schlecht
        //==== count 1 caret 0 start/ end 2 - start 0     = > wird damit abgedeckt: src.getCaretPosition() != src.getSelectionStart()
        
        
        // ==== count 1 caret 14 start/ end 14 - start 0
        //number = d; //TODO von rechts nach links markieren bringt nun ein Problem .. es werden mehr zeichen geloecht als noetig
        System.out.println("==== count " + number + " caret " +  src.getCaretPosition() + " \t end "  + src.getSelectionEnd() + " - start " + src.getSelectionStart() );
        int es =  src.getSelectionEnd() - src.getSelectionStart();
        if (es != 0 && es < d) {
           System.out.println("Markierung stimmt nicht e-s " + es +  " d " + d);   
           number = d;
           System.out.println("count update: "  + number);     
        } 
        
        String tmp1; 
        tmp1 = src.getText();
        for (int i = 0; i < number; i++) {
            KeyEvent ke = new KeyEvent(src, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, k, ch, KeyEvent.KEY_LOCATION_STANDARD);
            Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(ke);

            KeyEvent kkk = new KeyEvent(src, KeyEvent.KEY_RELEASED, System.currentTimeMillis(), 0, k, ch, KeyEvent.KEY_LOCATION_STANDARD);
            Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(kkk);
        }
        System.out.println(tmp1); 
                
    }

    public void keypress(JTextField src) {
        for (int i = 0; i < text.length(); i++) {
            char ch = text.charAt(i);
            int keyCode = Keys.getInstance().getKeyCode((int) ch);

            KeyEvent k = new KeyEvent(src, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, keyCode, ch, KeyEvent.KEY_LOCATION_STANDARD);
            Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(k);

            KeyEvent kk = new KeyEvent(src, KeyEvent.KEY_TYPED, System.currentTimeMillis(), 0, 0, ch);
            Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(kk);

            KeyEvent kkk = new KeyEvent(src, KeyEvent.KEY_RELEASED, System.currentTimeMillis(), 0, keyCode, ch, KeyEvent.KEY_LOCATION_STANDARD);
            Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(kkk);
        }
    }

    public void shift(JTextField src, int srcCaret, int caret) {
        System.out.println("(shift) caret " +  src.getCaretPosition() + "  end "  + src.getSelectionEnd() + " - start " + src.getSelectionStart() );
        if(src.getSelectionEnd() - src.getSelectionStart() <= 0){
            System.out.println("falsch *** hier sollte jetzt keine verschiebung erfolgen!!!");  
        } 
        
        if(src.getSelectionEnd() - src.getSelectionStart() != 0){
            System.out.println("*** hier sollte jetzt keine verschiebung erfolgen!!!");  
        } 
        
        
        System.out.println("src " + srcCaret + "  " + caret);
        char ch = 65535;
        int keyCode = srcCaret > caret ? 37 : 39;
        int diff = Math.abs(srcCaret - caret);
        for (int i = 0; i < diff; i++) {
            System.out.println("caret " + caret + " " + diff + " " + keyCode);
            KeyEvent k = new KeyEvent(src, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, keyCode, ch, KeyEvent.KEY_LOCATION_STANDARD);
            Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(k);

            KeyEvent kkk = new KeyEvent(src, KeyEvent.KEY_RELEASED, System.currentTimeMillis(), 0, keyCode, ch, KeyEvent.KEY_LOCATION_STANDARD);
            Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(kkk);
        }
    }

    @Override
    public String toString() {
        return "Keyzz{" + "c=" + c + ", text=" + text + ", k=" + k + '}';
    }
}
