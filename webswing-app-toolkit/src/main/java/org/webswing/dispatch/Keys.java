package org.webswing.dispatch;

import java.util.HashMap;

public class Keys {

    private HashMap<Integer, Integer> keyMap; /* char, code */

    private static class SingletonHolder {
        private static final Keys INSTANCE = new Keys();
    }

    public static Keys getInstance() {
        return SingletonHolder.INSTANCE;
    }
    
    public int getKeyCode(int charCode) {
        return keyMap.get(charCode) != null ? keyMap.get(charCode) : 0;
    }

    private Keys() {
        keyMap = new HashMap<Integer, Integer>();
        keyMap.put(34, 50);  ///key = keydown Code .. valuse = keypress Code (javascript)
        keyMap.put(35, 163);
        keyMap.put(32, 32);
        keyMap.put(33, 49);
        keyMap.put(38, 54);
        keyMap.put(39, 163);
        keyMap.put(36, 52);
        keyMap.put(37, 53);
        keyMap.put(42, 171);
        keyMap.put(43, 171);
        keyMap.put(40, 56);
        keyMap.put(41, 57);
        keyMap.put(46, 190);
        keyMap.put(47, 55);
        keyMap.put(44, 188);
        keyMap.put(45, 173);
        keyMap.put(51, 99);
        keyMap.put(50, 98);
        keyMap.put(49, 97);
        keyMap.put(48, 96);
        keyMap.put(55, 103);
        keyMap.put(54, 102);
        keyMap.put(53, 101);
        keyMap.put(52, 100);
        keyMap.put(59, 188);
        keyMap.put(58, 190);
        keyMap.put(57, 105);
        keyMap.put(56, 104);
        keyMap.put(63, 63);
        keyMap.put(62, 60);
        keyMap.put(61, 48);
        keyMap.put(60, 60);
        keyMap.put(68, 68);
        keyMap.put(69, 69);
        keyMap.put(70, 70);
        keyMap.put(71, 71);
        keyMap.put(64, 81);
        keyMap.put(65, 65);
        keyMap.put(66, 66);
        keyMap.put(67, 67);
        keyMap.put(76, 76);
        keyMap.put(77, 77);
        keyMap.put(78, 78);
        keyMap.put(79, 79);
        keyMap.put(72, 72);
        keyMap.put(73, 73);
        keyMap.put(74, 74);
        keyMap.put(75, 75);
        keyMap.put(85, 85);
        keyMap.put(84, 84);
        keyMap.put(87, 87);
        keyMap.put(86, 86);
        keyMap.put(81, 81);
        keyMap.put(80, 80);
        keyMap.put(83, 83);
        keyMap.put(82, 82);
        keyMap.put(93, 57);
        keyMap.put(92, 63);
        keyMap.put(95, 173);
        keyMap.put(94, 160);
        keyMap.put(89, 89);
        keyMap.put(88, 88);
        keyMap.put(91, 56);
        keyMap.put(90, 90);
        keyMap.put(102, 70);
        keyMap.put(103, 71);
        keyMap.put(100, 68);
        keyMap.put(101, 69);
        keyMap.put(98, 66);
        keyMap.put(99, 67);
        keyMap.put(96, 192);
        keyMap.put(97, 65);
        keyMap.put(110, 78);
        keyMap.put(111, 79);
        keyMap.put(108, 76);
        keyMap.put(109, 77);
        keyMap.put(106, 74);
        keyMap.put(107, 75);
        keyMap.put(104, 72);
        keyMap.put(105, 73);
        keyMap.put(119, 87);
        keyMap.put(118, 86);
        keyMap.put(117, 85);
        keyMap.put(116, 84);
        keyMap.put(115, 83);
        keyMap.put(114, 82);
        keyMap.put(113, 81);
        keyMap.put(112, 80);
        keyMap.put(126, 171);
        keyMap.put(125, 48);
        keyMap.put(124, 60);
        keyMap.put(123, 55);
        keyMap.put(122, 90);
        keyMap.put(121, 89);
        keyMap.put(120, 88);
        keyMap.put(167, 51);
        keyMap.put(178, 50);
        keyMap.put(179, 51);
        keyMap.put(176, 160);
        keyMap.put(180, 192);
        keyMap.put(181, 77);
        keyMap.put(223, 63);
      /*  keyMap.put(50, 34); // key = keypress Code   .. valuse = keydown Code (javascript)
        keyMap.put(163, 35);
        keyMap.put(32, 32);
        keyMap.put(49, 33);
        keyMap.put(54, 38);
        keyMap.put(163, 39);
        keyMap.put(52, 36);
        keyMap.put(53, 37);
        keyMap.put(171, 42);
        keyMap.put(171, 43);
        keyMap.put(56, 40);
        keyMap.put(57, 41);
        keyMap.put(190, 46);
        keyMap.put(55, 47);
        keyMap.put(188, 44);
        keyMap.put(173, 45);
        keyMap.put(99, 51);
        keyMap.put(98, 50);
        keyMap.put(97, 49);
        keyMap.put(96, 48);
        keyMap.put(103, 55);
        keyMap.put(102, 54);
        keyMap.put(101, 53);
        keyMap.put(100, 52);
        keyMap.put(188, 59);
        keyMap.put(190, 58);
        keyMap.put(105, 57);
        keyMap.put(104, 56);
        keyMap.put(63, 63);
        keyMap.put(60, 62);
        keyMap.put(48, 61);
        keyMap.put(60, 60);
        keyMap.put(68, 68);
        keyMap.put(69, 69);
        keyMap.put(70, 70);
        keyMap.put(71, 71);
        keyMap.put(81, 64);
        keyMap.put(65, 65);
        keyMap.put(66, 66);
        keyMap.put(67, 67);
        keyMap.put(76, 76);
        keyMap.put(77, 77);
        keyMap.put(78, 78);
        keyMap.put(79, 79);
        keyMap.put(72, 72);
        keyMap.put(73, 73);
        keyMap.put(74, 74);
        keyMap.put(75, 75);
        keyMap.put(85, 85);
        keyMap.put(84, 84);
        keyMap.put(87, 87);
        keyMap.put(86, 86);
        keyMap.put(81, 81);
        keyMap.put(80, 80);
        keyMap.put(83, 83);
        keyMap.put(82, 82);
        keyMap.put(57, 93);
        keyMap.put(63, 92);
        keyMap.put(173, 95);
        keyMap.put(160, 94);
        keyMap.put(89, 89);
        keyMap.put(88, 88);
        keyMap.put(56, 91);
        keyMap.put(90, 90);
        keyMap.put(70, 102);
        keyMap.put(71, 103);
        keyMap.put(68, 100);
        keyMap.put(69, 101);
        keyMap.put(66, 98);
        keyMap.put(67, 99);
        keyMap.put(192, 96);
        keyMap.put(65, 97);
        keyMap.put(78, 110);
        keyMap.put(79, 111);
        keyMap.put(76, 108);
        keyMap.put(77, 109);
        keyMap.put(74, 106);
        keyMap.put(75, 107);
        keyMap.put(72, 104);
        keyMap.put(73, 105);
        keyMap.put(87, 119);
        keyMap.put(86, 118);
        keyMap.put(85, 117);
        keyMap.put(84, 116);
        keyMap.put(83, 115);
        keyMap.put(82, 114);
        keyMap.put(81, 113);
        keyMap.put(80, 112);
        keyMap.put(171, 126);
        keyMap.put(48, 125);
        keyMap.put(60, 124);
        keyMap.put(55, 123);
        keyMap.put(90, 122);
        keyMap.put(89, 121);
        keyMap.put(88, 120);
        keyMap.put(51, 167);
        keyMap.put(50, 178);
        keyMap.put(51, 179);
        keyMap.put(160, 176);
        keyMap.put(192, 180);
        keyMap.put(77, 181);
        keyMap.put(63, 223);-*/
    }

}
