package org.webswing.util;

import java.awt.Component;
import java.awt.Point;
import javax.swing.JRootPane;

public class CalcLocationOfComponent {

    public Point calcLocationInWindow(Component c) {
        Point p;
        if (!isInRootPanel(c)) {
            p = calcPositionReku(c, new Point(0, 0));
        } else {
            p = c.getLocation();
        }
        return p;
    }

    private Point calcPositionReku(Component c, Point p) {
        if (!isInRootPanel(c)) {
            p.x += c.getLocation().x;
            p.y += c.getLocation().y;
            calcPositionReku(c.getParent(), p);
        } else {
            p.x += c.getLocation().x;
            p.y += c.getLocation().y;
        }
        return p;
    }

    private boolean isInRootPanel(Component c) {
        return c.getParent() == null || c.getParent() instanceof JRootPane;
    }
}


 
