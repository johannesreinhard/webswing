package org.webswing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.Enumeration;
import javax.servlet.MultipartConfigElement;

import org.eclipse.jetty.jmx.MBeanContainer;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.server.ssl.SslSelectChannelConnector;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.Attributes;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.webapp.WebAppContext;
import org.webswing.util.Logger;

public class ServerMain {

    public static void main(String[] args) throws Exception {
        Configuration config = ConfigurationImpl.parse(args);
        System.setProperty(Constants.SERVER_PORT, config.getPort());
        System.setProperty(Constants.SERVER_HOST, config.getHost());
        if (config.getConfigFile() != null) {
            File configFile = new File(config.getConfigFile());
            if (configFile.exists()) {
                System.setProperty(Constants.CONFIG_FILE_PATH, configFile.toURI().toString());
            } else {
                Logger.error("Webswing configuration file " + config.getConfigFile() + " not found. Using default location.");
            }
        }
        if (config.getUsersFile() != null) {
            File usersFile = new File(config.getUsersFile());
            if (usersFile.exists()) {
                System.setProperty(Constants.USER_FILE_PATH, usersFile.toURI().toString());
            } else {
                Logger.error("Webswing users property file " + config.getUsersFile() + " not found. Using default location.");
            }
        }

        Server server = new Server();
        if (config.isHttps()) {

            if (config.isHttps() && (!config.isNotEmpty(config.getTruststore())
                    || //                  !config.isNotEmpty(config.getPassword()) || Password can be blank
                    !config.isNotEmpty(config.getKeystore()))) {
                System.err.println("Don't start Webswing with SSL, because Truststore or Keystore be missing.");
            }

            if (!config.certsExist()) {
                throw new FileNotFoundException("\n\t " + config.getPathToKeystore() + "\nor \n\t" + config.getPathToKeystore() + "\ndo not exists!");
            }

            SslContextFactory sslContextFactory = new SslContextFactory(config.getPathToKeystore());
            sslContextFactory.setKeyStorePassword(config.getPassword());
            sslContextFactory.setTrustStore(config.getPathToTruststore());
            sslContextFactory.setTrustStorePassword(config.getPassword());
            sslContextFactory.setNeedClientAuth(false);
            SslSelectChannelConnector conSSL = new SslSelectChannelConnector(sslContextFactory);
            conSSL.setPort(Integer.parseInt(config.getHttpsPort()));
            conSSL.setHost(config.getHost());
            if (config.isHttp()) {
                Connector conHttp = new SelectChannelConnector();
                conHttp.setPort(Integer.parseInt(config.getPort()));
                conHttp.setHost(config.getHost());
                server.setConnectors(new Connector[]{conSSL, conHttp});
            } else {
                server.setConnectors(new Connector[]{conSSL});
            }
        } else {
            Connector conHttp = new SelectChannelConnector();
            conHttp.setPort(Integer.parseInt(config.getPort()));
            conHttp.setHost(config.getHost());
            server.setConnectors(new Connector[]{conHttp});
        }

        //enable jmx
        MBeanContainer mbcontainer = new MBeanContainer(ManagementFactory.getPlatformMBeanServer());
        server.getContainer().addEventListener(mbcontainer);
        server.addBean(mbcontainer);

        //mbcontainer.addBean(Log.getLog());
        WebAppContext webapp = new WebAppContext();
        webapp.setContextPath("/");
        webapp.setWar(System.getProperty(Constants.WAR_FILE_LOCATION));
        webapp.setTempDirectory(new File(URI.create(System.getProperty(Constants.TEMP_DIR_PATH))));
        server.setHandler(webapp);
        server.start();
        exportWebXmlParameter();
        server.join();
    }
    
    /**
     * read web.xml and export some parameter. 
     * at the moment need the client the maxfilesize. this parameter will be save in the
     * maxfilesize.dat file. The client read this parameter. 
     */
    public static void exportWebXmlParameter() {
        BufferedReader input = null;
        String targetLine = "";
        String tmp = System.getProperty(Constants.TEMP_DIR_PATH);
        try {
            File file = new File(tmp.substring(tmp.indexOf("/") + 1, tmp.length()), "webapp/WEB-INF/web.xml");
            input = new BufferedReader(new FileReader(file));
            String str;
            int start = 0;
            while ((str = input.readLine()) != null) {
                if (str.contains("<description>UploadServlet</description>")) {
                    start = 1;
                }

                if (start == 1 && str.contains("<max-file-size>")) {
                    targetLine = str;
                }

                if (start == 1 && str.contains("</multipart-config>")) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            String fileSize;

            if (!targetLine.isEmpty()) {
                fileSize = targetLine.replace("<max-file-size>", "").replace("</max-file-size>", "");
            } else {
                fileSize = "10485760";
            }
            File dir = new File(tmp.substring(tmp.indexOf("/") + 1, tmp.length()), "webapp/");
            if(!dir.exists()) {
                dir.mkdirs();
            }
            File file = new File(dir, "maxfilesize.dat");
            BufferedWriter output = null;
            try {
                output = new BufferedWriter(new FileWriter(file));
                output.write(fileSize.trim());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (output != null) {
                    try {
                        output.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
    }
}
