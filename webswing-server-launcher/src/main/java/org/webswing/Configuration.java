package org.webswing;

import java.io.File;

public abstract class Configuration {

    private static Configuration singleton = new ConfigurationImpl();

    public abstract String getHost();

    public abstract String getPort();

    public abstract String getConfigFile();

    public abstract String getUsersFile();

    //CSS Props
    public abstract boolean isHttp();
    public abstract String getHttpPort();
    public abstract boolean isHttps();
    public abstract String getHttpsPort();
    public abstract String getTruststore();
    public abstract String getPassword();
    public abstract String getKeystore();

    public abstract String getPathToTruststore();
    public abstract String getPathToKeystore();
    public abstract boolean certsExist();
    public abstract boolean isNotEmpty(String value);

    public static Configuration getInstance() {
        return singleton;
    }

}