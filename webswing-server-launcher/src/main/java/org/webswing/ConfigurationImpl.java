package org.webswing;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

public class ConfigurationImpl extends Configuration {

    private String host = "localhost";
    private String port = "8080";
    private String configFile;
    private String usersFile;

    private boolean http = true;
    private boolean https = false;
    private String https_port = "443";
    private String truststore;
    private String password;
    private String keystore;

    public String getHttps_port() {
        return https_port;
    }

    public void setHttps_port(String https_port) {
    	this.https_port = isNotEmpty(https_port) ? https_port : this.https_port;
    }

    public void setHttp(Boolean http) {
        this.http = http;
    }

    public void setHttps(Boolean https) {
        this.https = https;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean isHttp() {
        return http;
    }

    @Override
    public String getHttpPort() {
        return port;
    }

    @Override
    public boolean isHttps() {
        return https;
    }

    @Override
    public String getHttpsPort() {
        return https_port;
    }

    @Override
    public String getTruststore() {
        return truststore;
    }

    public void setTruststore(String value) {
        truststore = value;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getKeystore() {
        return keystore;
    }

    public void setKeystore(String value) {
        this.keystore = value;
    }

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    protected void setHost(String host) {
    	this.host = isNotEmpty(host) ? host : this.host;
    }

    protected void setPort(String port) {
    	this.port = isNotEmpty(port) ? port : this.port;
    }

    public String getConfigFile() {
        return configFile;
    }

    public void setConfigFile(String configFile) {
        this.configFile = configFile;
    }

    public String getUsersFile() {
        return usersFile;
    }

    public void setUsersFile(String usersFile) {
        this.usersFile = usersFile;
    }


    @Override
    public String getPathToTruststore() {
        return System.getProperty("user.dir") + "/certs/" + truststore;
    }

    @Override
    public String getPathToKeystore() {
        return System.getProperty("user.dir") + "/certs/" + keystore;
    }

    @Override
    public boolean certsExist(){
        File ts = new File(getPathToTruststore());
        File ks = new File(getPathToKeystore());
        return ts.exists() && ks.exists();
    }

    public static Configuration parse(String[] args) {
        ConfigurationImpl cimpl = (ConfigurationImpl) Configuration.getInstance();
        // create the command line parser
        CommandLineParser parser = new PosixParser();

        // create the Options
        Options options = new Options();
        options.addOption("h", "host", true, "Local interface address where the web server will listen. (localhost)");
        options.addOption("p", "port", true, "Port where the web server will listen. (8080)");
        options.addOption("t", "temp", true, "Temp folder for the webswing server. (./tmp)");
        options.addOption("c", "config", true, "Configuration file name. (<webswing-server.war path>/webswing.config)");
        options.addOption("u", "users", true, "Users properties file name. (<webswing-server.war path>/users.properties)");
        options.addOption("f", "file", true, "Use Config File. (<webswing-server.war path>/webswing.properties)");
        options.addOption("d", "createtmpdir", false, "create new Temp dir (default = false)");

        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);

            if (line.getOptionValue('f') != null) {
                readPropertyFile(line.getOptionValue('f'), cimpl);
            } else {
                if (line.getOptionValue('h') != null) {
                    cimpl.setHost(line.getOptionValue('h'));
                }
                if (line.getOptionValue('p') != null) {
                    cimpl.setPort(line.getOptionValue('p'));
                }
                if (line.getOptionValue('c') != null) {
                    cimpl.setConfigFile(line.getOptionValue('c'));
                }
                if (line.getOptionValue('u') != null) {
                    cimpl.setUsersFile(line.getOptionValue('u'));
                }
            }
        } catch (ParseException exp) {
            System.out.println(exp.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("webswing", options);
        } catch (IOException fnf) {
            fnf.printStackTrace();
        }
        return cimpl;
    }

    @Override
    public boolean isNotEmpty(String value) {
        return value != null && value.length() > 0;
     }

    private static void readPropertyFile(String filename, ConfigurationImpl cimpl) throws IOException {
        Properties prop = new Properties();
        InputStream inputStream = new FileInputStream(new File(filename));
        prop.load(inputStream);

        cimpl.setHttp(Boolean.parseBoolean(prop.getProperty("org.webswing.server.http")));
        cimpl.setPort(prop.getProperty("org.webswing.server.http.port"));

        cimpl.setHttps(Boolean.parseBoolean(prop.getProperty("org.webswing.server.https")));
        cimpl.setHttps_port(prop.getProperty("org.webswing.server.https.port"));
        cimpl.setTruststore(prop.getProperty("org.webswing.server.https.truststore"));
        cimpl.setPassword(prop.getProperty("org.webswing.server.https.password"));
        cimpl.setKeystore(prop.getProperty("org.webswing.server.https.keystore"));

        cimpl.setHost(prop.getProperty("org.webswing.server.host"));

        //cimpl.setCreateNewTemp(Boolean.parseBoolean(prop.getProperty("org.webswing.server.createNewTemp")));

    }
}
