package org.webswing.server;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.net.URI;
import java.net.URLDecoder;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.jms.Connection;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSession;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Java;
import org.apache.tools.ant.types.Environment.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.webswing.Constants;
import org.webswing.model.c2s.JsonConnectionHandshake;
import org.webswing.model.s2c.JsonAppFrame;
import org.webswing.model.s2c.JsonLinkAction;
import org.webswing.model.s2c.JsonLinkAction.JsonLinkActionType;
import org.webswing.model.s2c.OpenFileResult;
import org.webswing.model.s2c.PrinterJobResult;
import org.webswing.model.s2c.SystemProperty;
import org.webswing.model.server.SwingApplicationDescriptor;
import org.webswing.server.handler.DirectoryServlet;
import org.webswing.server.handler.FileServlet;
import org.webswing.server.util.Los;
import org.webswing.server.util.ServerUtil;
import org.webswing.server.util.SwingAntTimestampedLogger;
import org.webswing.toolkit.WebToolkit;
import org.webswing.toolkit.WebToolkit6;
import org.webswing.toolkit.WebToolkit7;
import org.webswing.toolkit.WebToolkit8;

public class SwingJvmConnection implements MessageListener {

    private static final Logger log = LoggerFactory.getLogger(SwingJvmConnection.class);
    private static ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("vm://localhost");

    private Connection connection;
    private Session session;
    private MessageProducer producer;
    private MessageConsumer consumer;
    private WebSessionListener webListener;
    private Future<?> app;
    private String clientId;
    private JMXConnector jmxConnection;
    private String userNameInSwingInstance = null;
    private boolean isWaitingForResponse;
    private boolean useApplicationDefinedRootDir; 
    private ExecutorService swingAppExecutor = Executors.newSingleThreadExecutor();

    public SwingJvmConnection(JsonConnectionHandshake handshake, SwingApplicationDescriptor appConfig, WebSessionListener webListener) {
        this.webListener = webListener;
        this.clientId = handshake.clientId;
        this.useApplicationDefinedRootDir = appConfig.useApplicationDefinedRootDir();
        try {
            initialize();
            String args = handshake.appArgs != null ? handshake.appArgs : ""; 
            app = start(appConfig, args, handshake.desktopWidth, handshake.desktopHeight);
        } catch (JMSException e) {
            log.error("SwingJvmConnection:init", e);
        }
    }
    
    public String getUserNameInSwingInstance() {
        return userNameInSwingInstance;
    }

    public void setUserNameInSwingInstance(String u) {
        this.userNameInSwingInstance = u;
    }
    
    public boolean isWaitingForResponse() {
        return isWaitingForResponse;
    }

    public void setWaitingForResponse(boolean w) {
        this.isWaitingForResponse = w;
    }

    public boolean useApplicationDefinedRootDir() {
        return useApplicationDefinedRootDir;
    }
        
    public boolean isRunning() {
        if (app == null || app.isDone() || app.isCancelled()) {
            return false;
        } else {
            return true;
        }
    }

    private void initialize() throws JMSException {
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Queue producerQueue = session.createQueue(clientId + Constants.SERVER2SWING);
        Queue consumerQueue = session.createQueue(clientId + Constants.SWING2SERVER);
        consumer = session.createConsumer(consumerQueue);
        consumer.setMessageListener(this);
        producer = session.createProducer(producerQueue);
        connection.setExceptionListener(new ExceptionListener() {

            @Override
            public void onException(JMSException paramJMSException) {
                log.warn("JMS encountered an exception: " + paramJMSException.getMessage());
                try {
                    consumer.close();
                    producer.close();
                    session.close();
                    connection.close();
                } catch (JMSException e) {
                    log.error("SwingJvmConnection:initialize1", e);
                }
                try {
                    SwingJvmConnection.this.initialize();
                } catch (JMSException e) {
                    log.error("SwingJvmConnection:initialize2", e);
                }
            }
        });
    }

    public void send(Serializable o) {
        try {        	
            if(session instanceof ActiveMQSession) {
            	ActiveMQSession ses = (ActiveMQSession) session;
            	if (o instanceof String) {
                	if(!ses.isClosed()) {
                		producer.send(session.createTextMessage((String) o));
                	} 
                } else {
                	if(!ses.isClosed()) {
                		producer.send(session.createObjectMessage(o));
                	}
                }
            }
        } catch (JMSException e) {
            log.error("SwingJvmConnection:send", e);
        }
    }

    public void onMessage(Message m) {
        try {
            if (m instanceof ObjectMessage) {
                Object o = ((ObjectMessage) m).getObject();
                if (o instanceof PrinterJobResult) {
                    PrinterJobResult pj = (PrinterJobResult) ((ObjectMessage) m).getObject();
                    FileServlet.registerFile(pj.getPdf(), pj.getId(), 30, TimeUnit.MINUTES, webListener.getUser());
                    JsonAppFrame f = new JsonAppFrame();
                    JsonLinkAction linkAction = new JsonLinkAction(JsonLinkActionType.print, pj.getId());
                    f.setLinkAction(linkAction);
                    webListener.sendToWeb(f);
                }
                if (o instanceof OpenFileResult) {
                    OpenFileResult fr = (OpenFileResult) ((ObjectMessage) m).getObject();
                    String id = UUID.randomUUID().toString();
                    FileServlet.registerFile(fr.getF(), id, 30, TimeUnit.MINUTES, webListener.getUser());
                    JsonAppFrame f = new JsonAppFrame();
                    JsonLinkAction linkAction = new JsonLinkAction(JsonLinkActionType.file, id);
                    f.setLinkAction(linkAction);
                    webListener.sendToWeb(f);
                }
                if (o instanceof SystemProperty) {
                    System.out.println("************* SwingJvmConnection SystemProperty");
                    SystemProperty v = (SystemProperty) o;
                    userNameInSwingInstance = v.getValue();
                    isWaitingForResponse = false;
                    return;
                }
                webListener.sendToWeb(((ObjectMessage) m).getObject());
            } else if (m instanceof TextMessage) {
                String text = ((TextMessage) m).getText();
                if (text.equals(Constants.SWING_SHUTDOWN_NOTIFICATION)) {
                    // close(true); - handled by ant thread
                }
                if (text.startsWith(Constants.SWING_PID_NOTIFICATION) && this.jmxConnection == null) {
                    this.jmxConnection = getLocalMBeanServerConnectionStatic(text.substring(Constants.SWING_PID_NOTIFICATION.length()));
                }
            }
        } catch (Exception e) {
            log.error("SwingJvmConnection:onMessage", e);

        }

    }

    public void close(boolean withShutdownEvent) {
        try {
            consumer.close();
            producer.close();
            session.close();
            connection.close();
            if(jmxConnection!=null){
            	JMXConnector thisjmxConnection = jmxConnection;
            	jmxConnection=null;
            	thisjmxConnection.close();
            }
        } catch (Exception e) {
        	log.debug("SwingJvmConnection:close", e);
        }
        if (withShutdownEvent) {
            webListener.sendToWeb(Constants.SWING_SHUTDOWN_NOTIFICATION);
        }
        webListener.notifyClose();
    }

    public Future<?> start(final SwingApplicationDescriptor appConfig, final String args, final Integer screenWidth, final Integer screenHeight) {
        Future<?> future = swingAppExecutor.submit(new Callable<Object>() {

            public Object call() throws Exception {
                try {
                    Project project = new Project();
                    //set home directory
                    String dirString = appConfig.getHomeDir();
                    File homeDir = new File(dirString);
                    if (!homeDir.exists()) {
                        homeDir.mkdirs();
                    }
                    project.setBaseDir(homeDir);
                    //setup logging
                    project.init();
                    DefaultLogger logger = new SwingAntTimestampedLogger();
                    project.addBuildListener(logger);
                    PrintStream os = new PrintStream(new Los(clientId));
                    logger.setOutputPrintStream(os);
                    logger.setErrorPrintStream(os);
                    logger.setMessageOutputLevel(Project.MSG_INFO);
                    //System.setOut(new PrintStream(new DemuxOutputStream(project, false)));
                    //System.setErr(new PrintStream(new DemuxOutputStream(project, true)));
                    project.fireBuildStarted();
                    Throwable caught = null;
                    try {
                        Java javaTask = new Java();
                        javaTask.setTaskName(clientId);
                        javaTask.setProject(project);
                        javaTask.setFork(true);
                        javaTask.setFailonerror(true);
                        javaTask.setJar(new File(URI.create(ServerUtil.getWarFileLocation())));
                        javaTask.setArgs(appConfig.getArgs() + "  " +  args);
                        String webSwingToolkitJarPath = "\"" + URLDecoder.decode(WebToolkit.class.getProtectionDomain().getCodeSource().getLocation().getPath(), "UTF-8") + "\"";
                        String webSwingToolkitJarPathSpecific;
                        String webToolkitClass;
                        if (System.getProperty("java.version").startsWith("1.6")) {
                            webSwingToolkitJarPathSpecific = "\"" + URLDecoder.decode(WebToolkit6.class.getProtectionDomain().getCodeSource().getLocation().getPath(), "UTF-8") + "\"";
                            webToolkitClass = WebToolkit6.class.getCanonicalName();
                        } else if (System.getProperty("java.version").startsWith("1.7")) {
                            webSwingToolkitJarPathSpecific = "\"" + URLDecoder.decode(WebToolkit7.class.getProtectionDomain().getCodeSource().getLocation().getPath(), "UTF-8") + "\"";
                            webToolkitClass = WebToolkit7.class.getCanonicalName();
                        } else if (System.getProperty("java.version").startsWith("1.8")) {
                            webSwingToolkitJarPathSpecific = "\"" + URLDecoder.decode(WebToolkit8.class.getProtectionDomain().getCodeSource().getLocation().getPath(), "UTF-8") + "\"";
                            webToolkitClass = WebToolkit8.class.getCanonicalName();
                        } else {
                            log.error("Java version " + System.getProperty("java.version") + " not supported in this version. Check www.webswing.org for supported versions.");
                            throw new RuntimeException("Java version not supported");
                        }
                        String bootCp = "-Xbootclasspath/a:" + webSwingToolkitJarPathSpecific + File.pathSeparatorChar + webSwingToolkitJarPath;

                        if (!System.getProperty("os.name", "").startsWith("Windows")) {
                            //filesystem isolation support on non windows systems:
                            bootCp += File.pathSeparatorChar + webSwingToolkitJarPath.substring(0, webSwingToolkitJarPath.lastIndexOf(File.separator)) + File.separator + "rt-win-shell-1.6.0_45.jar\"";
                        }
                        log.info("Setting bootclasspath to: " + bootCp);
                        String debug = appConfig.isDebug() ? " -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=y " : "";
                        String aaFonts = appConfig.isAntiAliasText() ? " -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true " : "";
                        javaTask.setJvmargs(bootCp + debug + aaFonts + " -noverify -Dcom.sun.management.jmxremote " + appConfig.getVmArgs());

                        addTask(javaTask, Constants.SWING_START_SYS_PROP_CLIENT_ID, clientId);        
                        addTask(javaTask, Constants.SWING_START_SYS_PROP_CLASS_PATH, appConfig.generateClassPathString());
                        addTask(javaTask, Constants.TEMP_DIR_PATH, System.getProperty(Constants.TEMP_DIR_PATH));
                        
                        addTask(javaTask, Constants.SWING_START_SYS_PROP_MAIN_CLASS, appConfig.getMainClass()); 
                        addTask(javaTask, Constants.SWING_START_SYS_PROP_ISOLATED_FS, appConfig.isIsolatedFs() + "");
                        addTask(javaTask, Constants.SWING_START_SYS_PROP_ALLOW_DOWNLOAD, appConfig.isAllowDownload()+ "");
                        addTask(javaTask, Constants.SWING_START_SYS_PROP_ALLOW_UPLOAD, appConfig.isAllowUpload()+ "");
                        addTask(javaTask, Constants.SWING_START_SYS_PROP_ALLOW_DELETE, appConfig.isAllowDelete()+ "");
                        addTask(javaTask, Constants.SWING_START_SYS_PROP_USE_APPLICATION_DEFINED_ROOT_DIR, appConfig.useApplicationDefinedRootDir()+ ""); 

                        addTask(javaTask, Constants.SWING_SESSION_TIMEOUT_SEC, appConfig.getSwingSessionTimeout()+ "");
                        addTask(javaTask, "awt.toolkit", webToolkitClass);
                        addTask(javaTask, "java.awt.headless", "false");
                        addTask(javaTask, "java.awt.graphicsenv", "org.webswing.toolkit.ge.WebGraphicsEnvironment");
                        addTask(javaTask, "java.awt.printerjob", "org.webswing.toolkit.WebPrinterJob");
                        addTask(javaTask, Constants.SWING_SCREEN_WIDTH, ((screenWidth == null || screenWidth < Constants.SWING_SCREEN_WIDTH_MIN) ? Constants.SWING_SCREEN_WIDTH_MIN : screenWidth) + "");
                        addTask(javaTask, Constants.SWING_SCREEN_HEIGHT, ((screenHeight == null || screenHeight < Constants.SWING_SCREEN_HEIGHT_MIN) ? Constants.SWING_SCREEN_HEIGHT_MIN : screenHeight) + "");

                        javaTask.init();
                        javaTask.executeJava();
                    } catch (BuildException e) {
                        project.fireBuildFinished(caught);
                        throw e;
                    }
                    project.fireBuildFinished(caught);
                } catch (Exception e) {
                    close(true);
                    throw e;
                }
                close(true);
                return null;
            }
        });
        return future;
    }
    
    public void addTask(Java javaTask, String key, String value){
        Variable v = new Variable();
        v.setKey(key);
        v.setValue(value);
        javaTask.addSysproperty(v);       
    }

    public String getClientId() {
        return clientId;
    }

    @SuppressWarnings("restriction")
    private JMXConnector getLocalMBeanServerConnectionStatic(String pid) {
        try {
            String address = sun.management.ConnectorAddressLink.importFrom(Integer.parseInt(pid));
            JMXServiceURL jmxUrl = new JMXServiceURL(address);
            return JMXConnectorFactory.connect(jmxUrl);
        } catch (Exception e) {
            log.error("Failed to connect to JMX of swing instance with pid " + pid + ".", e);
        }
        return null;
    }

    public MBeanServerConnection getJmxConnection() {
        try {
            return jmxConnection.getMBeanServerConnection();
        } catch (IOException e) {
            log.error("Failed to connect to JMX of swing instance ", e);
        }
        return null;
    }

    public interface WebSessionListener {

        String getUser();

        void sendToWeb(Serializable o);

        void notifyClose();
    }
}
