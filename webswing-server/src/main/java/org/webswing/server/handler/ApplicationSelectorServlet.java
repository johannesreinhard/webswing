package org.webswing.server.handler;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.webswing.model.server.SwingApplicationDescriptor;
import org.webswing.server.ConfigurationManager;

public class ApplicationSelectorServlet extends HttpServlet {

    private static final long serialVersionUID = -4359050705016810024L;
    public static final String SELECTED_APPLICATION = "selectedApplication";
    public static final String SWING_ARGS = "swingArgs";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String appName = req.getPathInfo();
        boolean notFound = true;
        if (appName != null && appName.length() > 0) {
            appName = appName.startsWith("/") ? appName.substring(1) : appName;
            SwingApplicationDescriptor app = ConfigurationManager.getInstance().getApplication(appName);
            if (app != null) {
                req.getSession().setAttribute(SELECTED_APPLICATION, appName);
                Enumeration<String> obj = req.getParameterNames();
                String args = "";
                while (obj.hasMoreElements()) {
                    String key = obj.nextElement();
                    String value = req.getParameter(key);
                    args += key + " " + value + " ";
                }
                req.getSession().setAttribute(SWING_ARGS, args);
                resp.sendRedirect("/");
                notFound = false;
            }
        }
        if(notFound) {
			resp.sendRedirect("/messagebox.html?type=1&app=" + appName);
        }
    }
}
