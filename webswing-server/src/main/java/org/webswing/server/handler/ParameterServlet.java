package org.webswing.server.handler;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.shiro.SecurityUtils;
import org.webswing.model.s2c.SystemProperty;
import org.webswing.model.server.SwingApplicationDescriptor;
import org.webswing.server.ConfigurationManager;
import org.webswing.server.SwingInstance;
import org.webswing.server.SwingInstanceManager;

public class ParameterServlet extends HttpServlet {

	private static final long serialVersionUID = 7510963790722200536L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userId = (String) SecurityUtils.getSubject().getPrincipal();

        Cookie[] arrayOfCookie = req.getCookies();
        String webswingID = null;
        for (int i = 0; i < arrayOfCookie.length; i++) {
            Cookie c = arrayOfCookie[i];
            if (c.getName().equals("webswingID")) {
                webswingID = c.getValue();
                break;
            }
        }

        String appName = null;
        Enumeration<String> parameterNames = req.getParameterNames();
        String param = "";
        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            String[] paramValues = req.getParameterValues(paramName);
            for (int i = 0; i < paramValues.length; i++) {
                String paramValue = paramValues[i];
                if (paramName.equals("app")) {
                    appName = paramValue;
                } else {
					param += paramName + ";" + paramValue + ";";
                }
            }
        }

		SwingApplicationDescriptor sad = ConfigurationManager.getInstance().getApplication(appName);
		if (userId == null) {
			if (sad != null) {
				//resp.sendError(HttpServletResponse.SC_FORBIDDEN); // 403.
				resp.sendRedirect("/messagebox.html?type=2&app=" + appName + "&link=" + getUrlToStartApp(resp, param, appName));
			} else {
				//APP muss vorhanden sein
				resp.sendRedirect("/messagebox.html?type=1&app=" + appName);
			}
		} else {
			if (appName != null && sad != null && webswingID != null && !param.isEmpty()) {
				boolean appfound = sendParameterToSwing(userId, webswingID, appName, param);
				if (appfound) {
					resp.sendRedirect("/app/" + appName);
				} else {
					//APP muss vorhanden sein
					resp.sendRedirect("/messagebox.html?type=2&app=" + appName + "&link=" + getUrlToStartApp(resp, param, appName));
				}
			} else {
				//APP muss vorhanden sein
				resp.sendRedirect("/messagebox.html?type=1&app=" + appName);
			}
        }
    }

	private String getUrlToStartApp(HttpServletResponse resp, String param, String appName) throws IOException {
		String replace = "";
		String[] array = param.split(";");
		for (int i = 0; i < array.length; i += 2) {
			replace += array[i] + "=" + array[i + 1] + "&";
		}

		String link = "/app/" + appName + "?" + replace;
		byte[] base = Base64.encodeBase64(link.getBytes());
		
		return new String(base);
	}

	public void startSwingInstance(HttpServletResponse resp, String param, String appName) throws IOException {
		resp.sendRedirect(getUrlToStartApp(resp, param, appName));
	}

	private boolean sendParameterToSwing(String userId, String webswingId, String app, String param) {
        SwingInstanceManager sw = SwingInstanceManager.getInstance();
        SystemProperty json = new SystemProperty();
        json.setKey("swingparam");
        json.setValue(param);
        String clientID = userId + webswingId + app;
        boolean appFound = false;
        for (SwingInstance instance : sw.getSwingInstanceSet()) {
            if (instance.getClientId().equals(clientID)) {
                json.setClientId(instance.getClientId());
                SwingInstanceManager.getInstance().sendMessageToSwing(null, instance.getClientId(), json);
                System.out.println("session id instance " + instance.getSessionId());
                appFound = true;
                break;
            }
        }

		return appFound;
    }
}
