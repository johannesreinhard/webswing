function WebswingBase(c) {
    "use strict";
    var config = {
        send: c.send || function () {
        },
        onErrorMessage: c.onErrorMessage || function () {
        },
        onContinueOldSession: c.onContinueOldSession || function () {
        },
        onApplicationSelection: c.onApplicationSelection || function () {
        },
        onBeforePaint: c.onBeforePaint || function () {
        },
        onLinkOpenAction: c.onLinkOpenAction || function () {
        },
        onPrintAction: c.onPrintAction || function () {
        },
        onFileDownloadAction: c.onFileDownloadAction || function () {
        },
        onFileDialogAction: c.onFileDialogAction || function () {
        },
        fileUploadAck: c.fileUploadAck || function () {
        },
        clientId: c.clientId || '',
        hasControl: c.hasControl || false,
        mirrorMode: c.mirrorMode || false
    }

    var clientId = config.clientId;
    var appName = null;
    var appArgs = null;
    var uuid = null;
    var latestMouseMoveEvent = null;
    var latestMouseWheelEvent = null;
    var latestWindowResizeEvent = null;
    var canvas;
    var mouseDown = 0;
    var user = null;
    var canPaint = false;
    var mirrorMode = config.mirrorMode;
    var keyCodeDown;
	var redirect;

    var timer1 = setInterval(mouseMoveEventFilter, 100);
    var timer2 = setInterval(heartbeat, 10000);
    var timeoutKeyEvent;
    var isCopyPaste = false; //Event
    var copyPasteSelection = null;
    // var areKeyEventsSend = true; // TODO loeschen, wird nicht mehr gebraucht
    var keys = [];


    function send(message) {
        config.send(message);
    }

    function sendInput(message) {
        if (config.hasControl) {
            sendTextInput();
            send(message);
        }
    }

    function sendTextInput(inputID) {
        if (keys.length > 0) {
            clearTimeout(timeoutKeyEvent);
            var idx = inputID || "#" + $("#spezInput").find("input")[0].id;
            sendKeyEvents(idx, $(idx).caret());
        }
    }

    function heartbeat() {
        send('hb' + clientId);
    }

    function mouseMoveEventFilter() {
        if (latestMouseMoveEvent != null && config.hasControl) {
            send(latestMouseMoveEvent);
            latestMouseMoveEvent = null;
        }
        if (latestMouseWheelEvent != null && config.hasControl) {
            send(latestMouseWheelEvent);
            latestMouseWheelEvent = null;
        }
        if (latestWindowResizeEvent != null && config.hasControl) {
            send(latestWindowResizeEvent);
            latestWindowResizeEvent = null;
            $("#spezInput").remove();
            $("#canvas").focus();
        }
    }


    function setCanvas(c) {
        canvas = c;
        registerEventListeners(c);
    }

    function repaint() {
        send('repaint' + clientId);
    }

    function ack() {
        send('paintAck' + clientId);
    }

    function kill() {
        send('killSwing' + clientId);
    }

    function unload() {
        if (!("none" === $("#fileDialogTransferBar").css("display"))) {  // is visible?
            send({clientId: clientId, files: []}); //close filechooser 
        }
        send('unload' + clientId);
    }

    function requestDownloadFile() {
        send('downloadFile' + clientId);
    }

    function requestDeleteFile() {
        send('deleteFile' + clientId);
    }

    function handshake() {
        send(getHandShake());
    }

    function resizedWindow() {
        latestWindowResizeEvent = getHandShake();
    }

    function dispose() {
        $("#spezInput").remove();
        clearInterval(timer1);
        clearInterval(timer2);
        canvas.parentNode.replaceChild(canvas.cloneNode(true), canvas);
        unload();
        c = {};
    }

    function processTxtMessage(message) {		
		if (message == "shutDownNotification") {
			console.log('MESSAGE', redirect);
			if(redirect == null || redirect == undefined || redirect == "") {
				var pic = '<img height="80%" width="80%" src="img/css.jpg">';
				var content = pic + '<br\><br\><br\> Thank you for use our Application. Goodbye<br\><a href="javascript:webswing.continueSession(false);">Start new session.</a>'
				if(!clientId.startsWith('anonym')) {
					content = content + '<br\><a href="/logout">Logout.</a>';
				}
				config.onErrorMessage(content);
			} else {
				document.location.href=redirect;
			}
        } else if (message == "applicationAlreadyRunning") {
            config.onErrorMessage('Application is already running in other browser window...');
        } else if (message == "tooManyClientsNotification") {
            config.onErrorMessage('Too many connections. Please try again later...');
        } else if (message == "continueOldSession") {
            config.onContinueOldSession();
        } 
        return;
    }
	
	

    function processJsonMessage(data) {
        //console.log(data);

        if (data.type !== undefined) {
            if (data.user != null) {
                user = data.user;
            }
            if (data.applications != null) {
                config.onApplicationSelection(data.applications);
            }
            if (canPaint) {
                processRequest(data);
            }

        } else if (data.filename !== undefined) {
            config.fileUploadAck(data.filename);
        }

    }

    function processRequest(data) {
        config.onBeforePaint();
        var context;
        context = canvas.getContext("2d");
        if (data.linkAction != null) {
            if (data.linkAction.action == 'url') {
                config.onLinkOpenAction(data.linkAction.url);
            } else if (data.linkAction.action == 'print') {
                config.onPrintAction(data.linkAction.url);
            } else if (data.linkAction.action == 'file') {
                config.onFileDownloadAction(data.linkAction.url);
            }
        }
        if (data.moveAction != null) {
            copy(data.moveAction.sx, data.moveAction.sy, data.moveAction.dx, data.moveAction.dy, data.moveAction.width, data.moveAction.height, context);
        }
        if (data.cursorChange != null && config.hasControl) {
            canvas.style.cursor = data.cursorChange.cursor;
        }
        if (data.copyEvent != null && config.hasControl) {
            //window.prompt("Copy to clipboard: Ctrl+C, Enter", data.copyEvent.content);

            $("#clipboard").text(data.copyEvent.content).html()
            //  $("#clipboard").html(data.copyEvent.content);
        }
        if (data.fileDialogEvent != null && config.hasControl) {
            config.onFileDialogAction(data.fileDialogEvent);
        }
        // firs is always the background
        for (var i in data.windows) {
            var win = data.windows[i];
            if (win.id == 'BG') {
                if (mirrorMode) {
                    adjustCanvasSize(win.width, win.height);
                }
                for (var x in win.content) {
                    var winContent = win.content[x];
                    if (winContent != null) {
                        clear(win.posX + winContent.positionX, win.posY + winContent.positionY, winContent.width, winContent.height, context);
                    }
                }
                data.windows.splice(i, 1);
                break;
            }
        }
        // regular windows (background removed)
        for (var i in data.windows) {
            var win = data.windows[i];
            for (var x in win.content) {
                var winContent = win.content[x];
                if (winContent != null) {
                    draw(win.posX + winContent.positionX, win.posY + winContent.positionY, winContent.base64Content, context);
                }
            }
        }
        if (data.htmlComp !== null) { //TODO 
            // console.log(data.htmlComp);
            // wenn der typ = null dann soll geloescht werden, wenn != null, dann entweder ist es neu oder ein update
            var hc = data.htmlComp;
            $("#canvas").focus();
            $("#spezInput").remove();
            if (hc.typ !== null) {
                $("#body").append("<span id='spezInput'>");
                $("#spezInput").append('<input id="' + hc.id + '" type="' + hc.typ + '" value="" class="spezInput">');

                var idx = "#" + hc.id;
                $(idx).val(hc.content);
                var font = hc.fontfam === "Dialog" ? "Arial" : hc.fontfam;
                font = font === "SansSerif" ? "Sans-Serif" : font;
                $(idx).css("font-family", font);
                $(idx).css("font-size", hc.fontSize);
                $(idx).css("line-height", "normal");
                $(idx).css("outline", "none");
                $(idx).css("text-align", hc.align);
                $(idx).css("border", "1px solid #AFEBD4"); //#AFEBD4  // TODO red
                $(idx).css("left", hc.x + "px");
                $(idx).css("top", hc.y + "px"); //$(idx).css("top", (hc.y + 30) + "px"); 
                $(idx).css("width", hc.w + "px");
                $(idx).css("height", hc.h + "px");

                $(idx).focus();
                if (hc.selectionEnd > 0) {
                    $(idx).range(hc.selectionStart, hc.selectionEnd);
                } else {
                    $(idx).caret(hc.caret); // set cursor 
                }

                $(idx).data('oldVal', $(idx).val());
                $(idx).bind("propertychange change click keyup input paste", function (event) {
                    if (timeoutKeyEvent !== 'undefined') {
                        clearTimeout(timeoutKeyEvent);
                    }

                    var _timeout = 400;
                    var selection = $(idx).getSelection();
                    //  console.log($(idx).data('oldVal') + " <==> " + $(idx).val() + " == " + ($(idx).data('oldVal') === $(idx).val()) + " type " + event.type + " selection s " + selection.start + " e " + selection.end);
                    if (event.type === "paste") {
                        copyPasteSelection = {start: selection.start, end: selection.end};
                    }


                    if (selection.start - selection.end !== 0 && $(idx).data('oldVal') !== $(idx).val()) {
                        //verhindert, dass durch Mause-move der Inhalt veraendert wird, da in Java die Funktion fehlt
                        $(idx).val($(idx).data('oldVal'));
                    }

                    // console.log("copy paste ? " + (event.type === "input" && isCopyPaste));
                    if ($(idx).data('oldVal') !== $(idx).val() || (event.type === "input" && isCopyPaste)) {
                        if (isCopyPaste) {
                            copyPasteEvent(idx, $(idx).data('oldVal'), $(idx).val());
                            isCopyPaste = false;
                            copyPasteSelection = null;
                        }
                        $(idx).css("border", "1px solid #FFCCFF");
                        $(idx).data('oldVal', $(idx).val());
                        //  areKeyEventsSend = false;
                    }
                    timeoutKeyEvent = setTimeout(function () {
                        sendKeyEvents(idx, $(idx).caret()); //TODO sendKeyEvents() in sendInput aufrufen wenn areKeyEventsSend_>     
                    }, _timeout);
                });

                $(idx).keypress(function (evt) {
                    // Firefox Filter
                    var kc = evt.keyCode || evt.which || evt.key.charCodeAt(0);
                    if (evt.charCode !== 0 && (evt.ctrlKey && evt.altKey || !(evt.ctrlKey || evt.altKey))) {
                        if (!sollZeilen_nicht_Loeschen(evt, kc)) {
                             console.log("*************** xxxxxxxxxxxxx loeschen  *********************");
                            var  selection =  $(idx).getSelection();
                            for (var i = 0; i < selection.end - selection.start; i++) {
                                keys.push({t: "d", "k": 8, "c": selection.end - i});  //t: "d" keydown; t: "p" keypress
                            }
                        
                        }
                        console.log("push !!! " + kc);
                        keys.push({t: "p", "k": kc, "c": $(idx).caret()});  //t: "d" keydown; t: "p" keypress
                    }
                });

                $(idx).keydown(function (evt) {
                    var kc = evt.keyCode;

                    console.log("++++ + + + ++ " + kc);
                    // statt kc == 90 .. muss man x v c a ausschliessen 
                    // if (kc === 9 || kc === 13 || (kc >= 37 && kc <= 40) || (evt.ctrlKey && kc === 90)) { // TODO erweitern
                    if (kc === 9 || kc === 13 || (kc >= 37 && kc <= 40) || (evt.ctrlKey && !(evt.ctrlKey && evt.altKey) && kc !== 17 && kc !== 65 && kc !== 67 && kc !== 86 && kc !== 88)) {
                        //  13 ENTER; 9 Tab; 37 bis 40 Pfeiltasten
                        if (!(kc >= 37 && kc <= 40)) {
                            evt.preventDefault();
                            evt.stopPropagation();
                        }

                        // wichtig kc=37 bis 40 muessen hier verschickt werden
                        var keyevt = getKBKey('keydown', canvas, evt);
                        sendInput(keyevt);
                        //console.log(keyevt);
                    }

                    var selection = $(idx).getSelection();
                    //console.log(selection.start - selection.end);
                    var xsollZeilen_nicht_Loeschen = sollZeilen_nicht_Loeschen(evt, kc);
                    if ((kc >= 112 && kc <= 123) || kc === 93) { // F1 ... F12
                        xsollZeilen_nicht_Loeschen = true;
                        var keyevt = getKBKey('keydown', canvas, evt);
                        sendInput(keyevt);
                        console.log("5 sollZeilen_nicht_Loeschen");
                    } 
                    
                    
                    if (!xsollZeilen_nicht_Loeschen) {
                        console.log("*************** loeschen *********************");
                        if ((evt.keyCode === 8 || evt.keyCode === 46) && (selection.start - selection.end === 0)) {
                             console.log("*************** 1 *********************");
                            keys.push({t: "d", "k": evt.keyCode, "c": $(idx).caret()});  //t: "d" keydown; t: "p" keypress 
                        }
         
                        if((kc === 8 || kc === 46 || ( (evt.ctrlKey && (kc == 86 || kc == 88) )))) {
                            for (var i = 0; i < selection.end - selection.start; i++) {
                                keys.push({t: "d", "k": 8, "c": selection.end - i});  //t: "d" keydown; t: "p" keypress
                            }
                        }
                    } else {
                        console.log("------------------- nicht loeschen *********************");
                        // TODO das kommt hier weg!! weil stopBlabla nicht fuer weniger tasten greifen muss, wie man schon bei 37 bis 40 sieht.
                        //pfeiltasten zulassen und  strg+ Zeichen genauso alt+ zeichen
                        if (!(kc >= 37 && kc <= 40) && !(evt.ctrlKey || evt.altKey) && (kc !== 17 && kc !== 18)) {
                            evt.preventDefault();
                            evt.stopPropagation();
                        }
                    }
                    if (evt.ctrlKey && kc === 86) {
                        console.log("###### ctrl+v " + kc);
                        isCopyPaste = true;
                    }

                    if (evt.altKey && (kc === 18 || (kc >= 96 && kc <= 105))) {
                        evt.preventDefault();
                        evt.stopPropagation();
                        console.log("alt + nummernblock " + kc);
                    }
                });

                $(idx).on('mouseup', function () {
                    sendTextSelection(hc.id);
                });

                $(idx).contextmenu(function (evt) {
                    sendTextSelection(hc.id);
                    evt.preventDefault();
                    evt.stopPropagation();
                });

                $(idx).mouseup(function (evt) {
                    if (evt.which === 3) {
                        //TODO das hier vllt nach -> $(idx).contextmenu(function (evt) { }  verschieben. Da rechte Maustaste bei linkshaenderlner..  usw.
                        // ausserdem die Context-Taste auf der Tastatur springt dort auch an.
                        //sendTextSelection(idx);
                        var mousePos = getMousePos(canvas, evt, 'mouseup');
                        sendInput(mousePos);
                        //TODO Infos mitschicken
                    }
                });
            }
        }
        ack();
    }

    function sendKeyEvents(idx, currentCaret) {
        var c = -1; //caret
        var message = [];
        var m = -1; // TODO m braucht man nicht, weil message.length-1 == m
        var tmp; //TODO
        var tmpKeyCode;
        // if (!areKeyEventsSend && keys.length > 0) 
        if (keys.length > 0) {
            for (var i in keys) {
                if (keys[i].t !== "d") {
                    if (c === keys[i].c && message[m].text !== undefined) {
                        c++;
                        message[m].text += String.fromCharCode(keys[i].k);
                    } else {
                        console.log("=> " + JSON.stringify(message));
                        c = keys[i].c + 1;
                        message.push({c: keys[i].c, text: String.fromCharCode(keys[i].k)});
                        m++;
                    }
                } else {
                    //					  wechsel von 46 zu 28 und umgekehrt
                    if (c === keys[i].c && message[m].k !== undefined && tmpKeyCode === keys[i].k) {
                        tmp = c; ///*** TODO
                        c = keys[i].k === 8 ? (c - 1 < 0 ? 0 : c - 1) : c;
                        message[message.length - 1].d++;
                        // console.log("* " + c + " " + tmp); ///*** TODO
                    } else {
                        tmp = c; ///*** TODO
                        c = keys[i].k === 8 ? (keys[i].c - 1 < 0 ? 0 : keys[i].c - 1) : keys[i].c;
                        console.log("=> " + JSON.stringify(message));  ///***  TODO
                        console.log("' " + c + " " + tmp + " " + keys[i].c);  ///***  TODO 
                        message.push({c: keys[i].c, d: 1, k: keys[i].k});
                        tmpKeyCode = keys[i].k;
                        m++;
                    }
                }
            }
            keys = [];
            //areKeyEventsSend = true;
            //console.log({clientId: clientId, k: message, c: currentCaret});
            console.log(JSON.stringify({k: message, c: currentCaret}));
            send({clientId: clientId, k: message, c: currentCaret});
            $(idx).css("border", "1px solid red");
        }
    }

    function copyPasteEvent(idx, oldVal, newVal) {
        console.log("copyPasteEvent");
        var start = 0;
        var end = 0;

        var selection = copyPasteSelection;// TODO
        if (oldVal !== newVal) {
            for (start = 0; start < $(idx).caret(); start++) {
                if (oldVal.length <= start || oldVal.charAt(start) !== newVal.charAt(start)) {
                    break;
                }
            }
            end = $(idx).caret();
            if (copyPasteSelection.start < start) {
                start = copyPasteSelection.start;
            }
            if (end - start === 0) {
                start = copyPasteSelection.start;
            }
        } else {
            var selection = copyPasteSelection;// $(idx).getSelection();
            if (selection.start - selection.end !== 0) {
                start = selection.start;
                end = selection.end;
            }
        }
        for (var i = start; i < end; i++) {
            keys.push({t: "p", "k": newVal.charCodeAt(i), "c": i});  //t: "d" keydown; t: "p" keypress
        }
    }


    function sollZeilen_nicht_Loeschen(evt, kc) {

        var sollZeilen_nicht_Loeschen = false;

        if (kc === 9 || kc === 13 || (evt.keyCode >= 37 && evt.keyCode <= 40)) {   // TODO erweitern
            sollZeilen_nicht_Loeschen = true;
            console.log("1 sollZeilen_nicht_Loeschen");
        }

        if (evt.altKey && (kc === 18 || (kc >= 96 && kc <= 105))) {
            sollZeilen_nicht_Loeschen = true;
            console.log("2 sollZeilen_nicht_Loeschen");
        }

        if (evt.ctrlKey && kc === 17) {
            sollZeilen_nicht_Loeschen = true;
            console.log("3 sollZeilen_nicht_Loeschen");
        }

        if (evt.shiftKey && kc === 16 || kc === 20) {
            sollZeilen_nicht_Loeschen = true;
            console.log("4 sollZeilen_nicht_Loeschen");
        }

       /* if ((kc >= 112 && kc <= 123) || kc === 93) { // F1 ... F12
            sollZeilen_nicht_Loeschen = true;
            console.log("5 sollZeilen_nicht_Loeschen");
        }*/


        if ((evt.ctrlKey || evt.altKey) && (kc !== 17 && kc !== 18)) {
            sollZeilen_nicht_Loeschen = true;
            console.log("6 sollZeilen_nicht_Loeschen");
        }

        if (evt.ctrlKey && evt.altKey && (kc !== 17 && kc !== 18)) { //altgr + zeichen zb. ~
            sollZeilen_nicht_Loeschen = false;
            console.log("6.1 sollZeilen_nicht_Loeschen");
        }
        // ^         || escape
        if (kc === 220 || kc === 27)
        {
            sollZeilen_nicht_Loeschen = true;
            console.log("7 sollZeilen_nicht_Loeschen");
        }

        // rollen taste = 145;  druck = 19; einfg = 45; 35 und 36 Pos1 und Ende; bild hoch runter = 33 , 34; windows-taste = 91: 144 Numlock
      if (kc === 145 || kc === 19 || kc === 45 || (kc >= 33 && kc <= 36) || kc === 91 || kc === 144)
        {
            //alert("weitere tasten rausssuchen die daf?r sorgen, dass bei markieren von text und dr?cken der entsprechenden taste der string in der forschleife beim keydown gels?cht wird");
            sollZeilen_nicht_Loeschen = true;
            console.log("8 sollZeilen_nicht_Loeschen");
        }

        if (evt.ctrlKey && kc === 86) { //86 = v ||  88 = x
            sollZeilen_nicht_Loeschen = false;
            console.log("9 sollZeilen_nicht_Loeschen");
        }

       

        if (kc === 88) { //86 = v ||  88 = x
            sollZeilen_nicht_Loeschen = false;
            console.log("10 sollZeilen_nicht_Loeschen");
        }

        if (evt.altKey && (kc === 18 || (kc >= 96 && kc <= 105))) {
          //   sollZeilen_nicht_Loeschen = false;
            console.log("11 sollZeilen_nicht_Loeschen");
        }

        return sollZeilen_nicht_Loeschen;
    }

    function sendTextSelection(inputID) {
        // TODO hier wird nichts mehr gesendet, da es sonst probleme gibt
        //var selection = $("#" + inputID).getSelection();
        //sendInput({clientId: clientId, id: inputID, start: selection.start, end: selection.end});
    }

    function adjustCanvasSize(width, height) {
        if (canvas.width != width || canvas.height != height) {
            canvas.width = width;
            canvas.height = height;
        }
    }

    function draw(x, y, b64image, context) {
        var imageObj;
        imageObj = new Image();
        imageObj.onload = function () {
            context.drawImage(imageObj, x, y);
            imageObj.onload = null;
            imageObj.src = '';
        };
        imageObj.src = 'data:image/png;base64,' + b64image;
    }

    function clear(x, y, w, h, context) {
        context.clearRect(x, y, w, h);
    }

    function copy(sx, sy, dx, dy, w, h, context) {
        var copy = context.getImageData(sx, sy, w, h);
        context.putImageData(copy, dx, dy);
    }

    function registerEventListeners(canvas) {
        bindEvent(canvas, 'mousedown', function (evt) {
            var mousePos = getMousePos(canvas, evt, 'mousedown');
            latestMouseMoveEvent = null;
            sendInput(mousePos);
            canvas.focus();
            return false;
        }, false);
        bindEvent(canvas, 'dblclick', function (evt) {
            var mousePos = getMousePos(canvas, evt, 'dblclick');
            latestMouseMoveEvent = null;
            sendInput(mousePos);
            canvas.focus();
            return false;
        }, false);
        bindEvent(canvas, 'mousemove', function (evt) {
            var mousePos = getMousePos(canvas, evt, 'mousemove');
            mousePos.button = mouseDown;
            latestMouseMoveEvent = mousePos;
            return false;
        }, false);
        bindEvent(canvas, 'mouseup', function (evt) {
            var mousePos = getMousePos(canvas, evt, 'mouseup');
            latestMouseMoveEvent = null;
            sendInput(mousePos);
            return false;
        }, false);
        // IE9, Chrome, Safari, Opera
        bindEvent(canvas, "mousewheel", function (evt) {
            var mousePos = getMousePos(canvas, evt, 'mousewheel');
            latestMouseMoveEvent = null;
            if (latestMouseWheelEvent != null) {
                mousePos.wheelDelta += latestMouseWheelEvent.wheelDelta;
            }
            latestMouseWheelEvent = mousePos;
            return false;
        }, false);
        // firefox
        bindEvent(canvas, "DOMMouseScroll", function (evt) {
            var mousePos = getMousePos(canvas, evt, 'mousewheel');
            latestMouseMoveEvent = null;
            if (latestMouseWheelEvent != null) {
                mousePos.wheelDelta += latestMouseWheelEvent.wheelDelta;
            }
            latestMouseWheelEvent = mousePos;
            return false;
        }, false);
        bindEvent(canvas, 'contextmenu', function (event) {
            event.preventDefault();
            event.stopPropagation();
            return false;
        });

        bindEvent(canvas, 'keydown', function (event) {
            // TODO Copy Paste ...
            var kc = event.keyCode;
            keyCodeDown = kc;
            //  some keys don't produce keypress-Events. //TODO


            if (!(kc === 32 || (kc >= 48 && kc <= 57) || kc === 32 || (kc >= 65 && kc <= 90) || (kc >= 186 && kc <= 192) || (kc >= 219 && kc <= 222) || (kc == 226) || (kc == 0) || (kc == 163) || (kc == 171) || (kc == 173) || (kc >= 96 && kc <= 111) || (kc == 60))) {
                event.preventDefault();
                event.stopPropagation();
                console.log("whitout keypress event");
            }

            if ((event.ctrlKey || event.altKey) && (kc !== 17 && kc !== 18)) {
                //if (!event.altKey) {
                event.preventDefault();
                event.stopPropagation();
                console.log("alt ctrl; whitout keypress event");
                //}
                var keyevt = getKBKey('keydown', canvas, event);

                sendInput(keyevt); // send:  ctrl + a ...
            } else if (kc === 16 || kc === 17 || kc === 18 || kc === 20) {
                // shift CapsLock alt strg
            } else if (kc === 32 || (kc >= 48 && kc <= 57) || (kc >= 65 && kc <= 90) || (kc >= 96 && kc <= 105)) {
                // 0 - 9 ; a-z | A-Z ; Numpad 0-9
            }
            else {
                var keyevt = getKBKey('keydown', canvas, event);
                sendInput(keyevt); // send:  F1, ...
            }
            return false;
        }, false);
        bindEvent(canvas, 'keypress', function (event) {
            event.preventDefault();
            event.stopPropagation();
            var keyevt = getKBKey('keypress', canvas, event);
            sendInput(keyevt);
            return false;
        }, false);
        bindEvent(canvas, 'keyup', function (event) {
            /* event.preventDefault();
             event.stopPropagation();
             var keyevt = getKBKey('keyup', canvas, event);
             sendInput(keyevt);*/
            return false;
        }, false);
        bindEvent(document, 'mousedown', function (evt) {
            if (evt.which == 1) {
                mouseDown = 1;
            }
        });
        bindEvent(document, 'mouseout', function (evt) {
            mouseDown = 0;
        });
        bindEvent(document, 'mouseup', function (evt) {
            if (evt.which == 1) {
                mouseDown = 0;
            }
        });
    }

    function getMousePos(canvas, evt, type) {
        var rect = canvas.getBoundingClientRect();
        var root = document.documentElement;
        // return relative mouse position
        var mouseX = evt.clientX - rect.left - root.scrollTop;
        var mouseY = evt.clientY - rect.top - root.scrollLeft;
        var delta = 0;
        if (type == 'mousewheel') {
            delta = -Math.max(-1, Math.min(1, (evt.wheelDelta || -evt.detail)));
        }
        return {
            clientId: clientId,
            x: mouseX,
            y: mouseY,
            type: type,
            wheelDelta: delta,
            button: evt.which,
            ctrl: evt.ctrlKey,
            alt: evt.altKey,
            shift: evt.shiftKey,
            meta: evt.metaKey
        };
    }

    function getKBKey(type, canvas, evt) {
        var char = evt.which;
        if (char == 0 && evt.key != null) {
            char = evt.key.charCodeAt(0);
        }
        var kk = evt.keyCode;
        if (kk == 0) {
            kk = char;
        }
        return {
            clientId: clientId,
            type: type,
            character: char,
            keycode: kk,
            alt: evt.altKey,
            ctrl: evt.ctrlKey,
            shift: evt.shiftKey,
            meta: evt.metaKey,
            altgr: evt.altGraphKey
        };
    }

    function getHandShake() {
        var handshake = {
            applicationName: appName,
            clientId: clientId,
            sessionId: uuid,
            appArgs: appArgs,
            desktopWidth: canvas.width,
            desktopHeight: canvas.height,
            mirrored: mirrorMode
        };
        return handshake;
    }

    function bindEvent(el, eventName, eventHandler) {
        el.addEventListener(eventName, eventHandler, false);
    }
	
	function setRedirect(c) {
        redirect = c;
    }

    return {
        repaint: function () {
            repaint();
        },
        ack: function () {
            ack();
        },
        kill: function () {
            kill();
        },
        handshake: function () {
            handshake();
        },
        requestDownloadFile: function () {
            requestDownloadFile();
        },
        requestDeleteFile: function () {
            requestDeleteFile();
        },
        setUuid: function (param) {
            uuid = param;
        },
        getUser: function () {
            return user;
        },
        resizedWindow: function () {
            resizedWindow();
        },
        setCanvas: function (c) {
            setCanvas(c);
        },
		setRedirect: function (c) {
            setRedirect(c);
        },
        setApplication: function (app) {
            appName = app;
        },
        setAppArgs: function (args) {
            appArgs = args;
        },
        getClientId: function () {
            return clientId;
        },
        setClientId: function (id) {
            clientId = id;
        },
        canPaint: function (bool) {
            canPaint = bool;
        },
        canControl: function (bool) {
            config.hasControl = bool;
        },
        processTxtMessage: function (message) {
            processTxtMessage(message);
        },
        processJsonMessage: function (data) {
            processJsonMessage(data);
        },
        dispose: function () {
            dispose();
        }
    };
}